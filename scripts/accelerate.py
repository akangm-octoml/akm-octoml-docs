from octomizer.client import OctomizerClient
from octomizer.model_variant import AutoTVMOptions, AutoschedulerOptions
import argparse

parser = argparse.ArgumentParser(description="Octomize a model variant")
parser.add_argument(
    "--model_variant",
    "-v",
    type=str,
    help="a specific model variant to octomize",
    required=True,
)
parser.add_argument(
    "--platform", "-p", type=str, help="a platform to benchmark on", required=True
)
parser.add_argument(
    "--kernel_trials",
    "-k",
    type=int,
    help="number of kernel trials",
    default=10,
    required=False,
)
parser.add_argument(
    "--autoscheduler",
    action="store_true",
    default=False,
    help="use autoscheduler",
    required=False
)
parser.add_argument(
    "--threads", "-t", type=int, help="TVM_NUM_THREADS"
)
parser.add_argument(
    "--runs_per_trial", "-m", type=int, help="num_runs_per_trial", required=True
)
parser.add_argument(
    "--trials", "-n", type=int, help="num_benchmark_trials", required=True
)

args = parser.parse_args()
client = OctomizerClient()
mv = client.get_model_variant(args.model_variant)

if args.autoscheduler:
    tuning_options = AutoschedulerOptions(trials_per_kernel=args.kernel_trials, early_stopping_threshold=args.kernel_trials)
else:
    tuning_options = AutoTVMOptions(kernel_trials=0, exploration_trials=args.kernel_trials, early_stopping_threshold=args.kernel_trials)

wf = mv.accelerate(
    platform=args.platform,
    num_benchmark_trials=args.trials,
    num_runs_per_trial=args.runs_per_trial,
    tvm_num_threads=args.threads,
    tuning_options=tuning_options
)
print(wf)
print("waiting...")
wf.wait()
print(wf)

