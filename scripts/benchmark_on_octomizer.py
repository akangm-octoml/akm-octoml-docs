#!/usr/bin/env python3

from __future__ import annotations

import itertools
import time
import uuid
from dataclasses import dataclass

import octomizer.client
from octomizer.model_variant import AutoschedulerOptions
import octomizer.models.onnx_model as onnx_model
import octomizer.project as project
from pathlib import Path

# MLPerf Models used in previous analysis can be downloaded a the links below:
#
# Resnet50: https://zenodo.org/record/2592612/#.YrTIHuzMIUF
# BERT:     https://zenodo.org/record/3733910#.YrTTbuzMIUG
# DLRM:     https://github.com/mlcommons/inference/tree/master/recommendation/dlrm/pytorch
#           https://dlrm.s3-us-west-1.amazonaws.com/models/tb0875_10M.onnx.tar

# List the filenames of the models that you would like to run
MODEL_FILES = ["all-MiniLM-L6-v2_fp16.onnx"]

# List the hardware targets that you want to run the models on
#HARDWARE_TARGETS = ["gcp_ice_lake_n2-standard-4", "gcp_ice_lake_n2-standard-8"]"gcp_ice_lake_n2-standard-96",
HARDWARE_TARGETS= ["aws_p3.2xlarge", "aws_c7g.16xlarge"]

# List the batch sizes you would like to run each model with
BATCH_SIZES = [1]

PROJECT_NAME = "Pre/post-processing"
PROJECT_UUID = "6fb3c30d-0581-4b4a-a2cc-0318faffa20b"

# A Scenario containes all of the variables for each model run to help organize the resulting metrics
#
#   Model Name - this is the model file that was run
#   Platform   - this is the hardware target
#   Batch size - this is the batch size used
#   Engine     - this is the engine that was used for the run (this will be either TVM or ONNX)
#   Thread Count - Thread pool size used during this run. Is: 2, vCPU/2 or max vCPU count.
@dataclass(frozen=True, eq=True)
class Scenario:
   model_name: str
   platform: str
   batch_size: int
   engine: str
   thread_count:int

# Define the input shapes for each model
def _model_to_inputs(model_name, batch_size):
   """Returns (input_shapes, input_dtypes)
   """
   if model_name == "all-MiniLM-L6-v2_fp16.onnx":
       return ({"input_ids": [batch_size, 128], "attention_mask": [batch_size, 128], "token_type_ids": [batch_size, 128]},
               {"input_ids": "int64", "attention_mask": "int64", "token_type_ids": "int64"})
   if model_name == "model.onnx":
       return ({'input_ids': [batch_size, 384], 'input_mask': [batch_size, 384], 'segment_ids': [batch_size, 384]},
               {"input_ids":"int64", "input_mask":"int64", "segment_ids":"int64"})
   if model_name == "resnet50_v1.onnx":
       return ({"input_tensor:0": [batch_size, 3, 224, 224]},
               {"input_tensor:0":"float32"})
   if model_name == "tb0875_10M.onnx.tar":
       return ({'input.1': [batch_size, 13], 'lS_o': [26, batch_size], 'lS_i': [26, batch_size]},
               {'input.1': 'float32', 'lS_o': 'int64', 'lS_i': 'int64'})


def _thread_pool_size(platform,hw_list):
   hw_profile_item = [x for x in hw_list if x.platform==platform][0]
   avail_vcpus = hw_profile_item.vcpu_count
   thread_pool_sizes = sorted(list(set([2,int(avail_vcpus/2),avail_vcpus])))
   print(f"{avail_vcpus} vCPUs available for {platform}")
   return(thread_pool_sizes)


def _lookup_uploaded_model(model_unique_name,project_models):
   project_model_item = [x for x in project_models if x.proto.name==model_unique_name][0]
   return(project_model_item)



if __name__ == "__main__":
   # Invoke hardware-map list from Octomizer client.
   client = octomizer.client.OctomizerClient(access_token="/HWiZyhlJ2eUdvqKd1IwfQ==")

   print(f"Creating or starting project {PROJECT_NAME}.")
   if PROJECT_UUID:
       my_project = client.get_project(PROJECT_UUID)
   else:
       my_project = project.Project(
           client,
           name=PROJECT_NAME
       )

   # Get list of HW targets available for optimization and acceleration
   HW_LIST = list(client.get_hardware_targets())

   # Get list of models already uploaded to project
   PROJECT_MODELS = list(my_project.list_models())
   MODEL_UUIDS = [x.uuid for x in PROJECT_MODELS]
   MODEL_NAMES = [x.proto.name for x in PROJECT_MODELS]

   # Upload the ONNX model file.
   models = []
   for model_name in MODEL_FILES:
       #uid = str(uuid.uuid4())[:8]
       #unique_name = f"{model_name}-{uid}"
      
       # Skip re-uploading the model if it already exists.

       if model_name not in MODEL_NAMES:
           print(f"Uploading model {model_name}...")
           model = onnx_model.ONNXModel(
           client,
           name=model_name,
           model=model_name,
           description=model_name,
           project=my_project)

       else:
           print(f"Model already exists as {model_name}")
           model = _lookup_uploaded_model(model_name,PROJECT_MODELS)

       models.append((model_name, model.get_uploaded_model_variant()))

   # For each combination of model, platform, batch_size, and tvm_thread_num,
   # run one TVM acceleration and benchmarking job.
   # and one ONNX-RT benchmarking job.
   product = itertools.product(models, HARDWARE_TARGETS, BATCH_SIZES)
   workflows = {}
   for model, platform, batch_size in product:
       model_name, model_variant = model
       if model_name == "tb0875_10M.onnx.tar" and batch_size != 1:
           # this DLRM model doesn't have dynamic inputs
           # Skip varying batch size.
           continue
       input_shapes, input_dtypes = _model_to_inputs(model_name, batch_size)

       # Test three different thread sizes, maximum, vCPUs/2, and 2.
       thread_sizes = _thread_pool_size(platform,HW_LIST)
       for thread_count in thread_sizes:
           print(thread_count)
          
       # ONNX benchmark
           scenario = Scenario(model_name, platform, batch_size,thread_count, "onnx")
           workflow = model_variant.benchmark(platform=platform,
                           input_shapes=input_shapes,
                           input_dtypes=input_dtypes,
                           use_onnx_engine=True,
                           create_package=True,
                           tvm_num_threads=thread_count)

           workflows[scenario] = workflow
          
           # Tuned TVM acceleration
           scenario = Scenario(model_name, platform, batch_size, thread_count, "tvm")
           autoscheduler_options = AutoschedulerOptions(trials_per_kernel=1000,
                                               early_stopping_threshold=250)

           workflow = model_variant.accelerate(platform,
                                   tuning_options=autoscheduler_options,
                                   input_shapes=input_shapes,
                                   input_dtypes=input_dtypes,
                                   tvm_num_threads=thread_count
                                   )
          
           workflows[scenario] = workflow
      
   # Wait until all workflows terminate.
   for workflow in workflows.values():
       while not workflow.done():
           time.sleep(1)
  
   completed_workflows = 0
   failed_workflows = 0
   for workflow in workflows.values():
       if workflow.completed():
           completed_workflows += 1
       else:
           failed_workflows += 1
   print(f"{completed_workflows} completed. {failed_workflows} failed.")

   # Coalesce the results
   with open("benchmark_results.csv", "w") as f:
       f.write("id,workflow_uid,model_name,platform,batch_size,engine,latency,latency_stddev,throughput,thread_count\n")
       uid = 0
       for scenario, workflow in workflows.items():
           if not workflow.completed():
               print(f"Skipping failed workflow with id {workflow.uuid}")
               continue
           uid += 1
           latency = workflow.metrics().latency_mean_ms
           latency_stddev = workflow.metrics().latency_std_ms
           # throughput in samples / sec
           throughput = scenario.batch_size * (1000 / latency)
           thread_count = scenario.thread_count
           f.write(f"{uid},")
           f.write(f"{workflow.uuid},")
           f.write(f"{scenario.model_name},")
           f.write(f"{scenario.platform},")
           f.write(f"{scenario.batch_size},")
           f.write(f"{scenario.engine},")
           f.write(f"{latency},")
           f.write(f"{latency_stddev},")
           f.write(f"{throughput}")
           f.write(f"{thread_count}\n")

