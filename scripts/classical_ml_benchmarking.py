import time

import numpy as np
import onnxruntime as ort
import xgboost as xgb
from onnxmltools.convert.xgboost.operator_converters.XGBoost import convert_xgboost
from sklearn.datasets import make_blobs
from skl2onnx.common.shape_calculator import calculate_linear_classifier_output_shapes
from skl2onnx import update_registered_converter

from hulk.worker.models.sklearn_model import SklearnModel


def initialize():
    update_registered_converter(
        xgb.XGBClassifier,
        "XGBoostXGBClassifier",
        calculate_linear_classifier_output_shapes,
        convert_xgboost,
        options={'nocl': [True, False], 'zipmap': [True, False]}
    )


def train_xgboost(num_features):
    X, y = make_blobs(n_samples=1000,
               n_features=num_features,
               cluster_std=1.5,
               random_state=1)
    model = xgb.XGBClassifier()
    model.fit(X, y)
    sklearn_model = SklearnModel(model)
    onnx_model = sklearn_model.to_onnx()
    return sklearn_model, onnx_model



def benchmark_sklearn(model, samples_per_batch):
    input_data = np.ones((samples_per_batch, model.model.n_features_in_))
    now = time.time()
    model.model.predict(input_data)
    later = time.time()
    elapsed = later - now
    return elapsed

def benchmark_onnx(model, samples_per_batch, ort_sess):
    input_data = np.ones((samples_per_batch, model.input_shapes['input'][0]), dtype=np.int64)
    now = time.time()
    ort_sess.run(None, {'input': input_data})
    later = time.time()
    elapsed = later - now
    return elapsed

    #"visa_xgboost_classifier.joblib"
    #"visa_xgboost_classifier.onnx"
if __name__=="__main__":
    initialize()
    # Model with one feature.
    results = []
    for num_features in [1, 10, 100, 1000, 10000]:
        for num_samples in [1, 10, 100, 1000, 10000, 100000]:
            sklearn_model, onnx_model = train_xgboost(num_features)
            ort_sess = ort.InferenceSession(onnx_model.model.SerializeToString(), providers=["CPUExecutionProvider"])
            
            sklearn_latency  = benchmark_sklearn(sklearn_model, num_samples)
            onnx_latency  = benchmark_onnx(onnx_model, num_samples, ort_sess)
            results.append(['sklearn', num_features, num_samples, sklearn_latency])
            results.append(['onnx', num_features, num_samples, onnx_latency])

            print("Sklearn latency, {} samples, {} features: {}".format(num_samples, num_features, benchmark_sklearn(sklearn_model, num_samples)))
            print("ONNX latency, {} samples, {} features: {}".format(num_samples, num_features, benchmark_onnx(onnx_model, num_samples, ort_sess)))
    result_str = "model_type,num_features,num_samples,latency\n"
    for result_line in results:
        s = f"{result_line[0]},{result_line[1]},{result_line[2]},{result_line[3]}\n"
        result_str += s
    with open("results_cuda.csv", "w") as f:
        f.write(result_str)
