import time
from dataclasses import dataclass
import numpy as np
import onnxruntime as ort
from onnxruntime import RunOptions
import xgboost as xgb
from onnxmltools.convert.xgboost.operator_converters.XGBoost import convert_xgboost
from sklearn.datasets import make_blobs
from skl2onnx.common.shape_calculator import calculate_linear_classifier_output_shapes
from skl2onnx import update_registered_converter
from sklearn.linear_model import LinearRegression, LogisticRegression
from sklearn.gaussian_process.kernels import RBF
from sklearn.neighbors import KNeighborsClassifier
from sklearn.neural_network import MLPClassifier, MLPRegressor
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier
from sklearn.naive_bayes import BernoulliNB
from onnxmltools.convert.xgboost.operator_converters.XGBoost import convert_xgboost
from skl2onnx import convert_sklearn, update_registered_converter
from skl2onnx.common.data_types import (
    BooleanTensorType,
    Float16TensorType,
    FloatTensorType,
    Int8TensorType,
    Int16TensorType,
    Int32TensorType,
    Int64TensorType,
    UInt8TensorType,
    UInt16TensorType,
    UInt32TensorType,
    UInt64TensorType,
)
from skl2onnx.common.exceptions import MissingShapeCalculator
from skl2onnx.common.shape_calculator import (
    calculate_linear_classifier_output_shapes,
    calculate_linear_regressor_output_shapes,
)

_DTYPE_MAP = {
    np.dtype( "bool8" ) : BooleanTensorType,
    np.dtype( "int8" ) : Int8TensorType,
    np.dtype( "int16" ) : Int16TensorType,
    np.dtype( "int32" ) : Int32TensorType,
    np.dtype( "int64" ) : Int64TensorType,
    np.dtype( "uint8" ) : UInt8TensorType,
    np.dtype( "uint16" ) : UInt16TensorType,
    np.dtype( "uint32" ) : UInt32TensorType,
    np.dtype( "uint64" ) : UInt64TensorType,
    np.dtype( "float16" ) : Float16TensorType,
    np.dtype( "float32" ) : FloatTensorType,
    np.dtype( "float64" ) : FloatTensorType,
}

def initialize():
    update_registered_converter(
        xgb.XGBClassifier,
        "XGBoostXGBClassifier",
        calculate_linear_classifier_output_shapes,
        convert_xgboost,
        options={'nocl': [True, False], 'zipmap': [True, False]}
    )
    update_registered_converter(
        xgb.XGBRegressor,
        "XGBoostXGBRegressor",
        calculate_linear_regressor_output_shapes,
        convert_xgboost,
        options={'nocl': [True, False], 'zipmap': [True, False]}
    )


def sklearn_to_onnx(sklearn_model, input_dtype, num_features):
    # initial_type = [('input', FloatTensorType([None, 4]))]

    name = "input"
    tensor_type = _DTYPE_MAP[input_dtype]
    initial_type = [(name, tensor_type([None, num_features]))]

    # "None" signifies variable length input in skl2onnx.
    # onnx_shapes = [None] + input_shapes[self._INPUT_NAME]  # type: ignore
    # convert_sklearn_type = self._DTYPE_MAP[input_dtypes[self._INPUT_NAME]]
    # initial_type = [(self._INPUT_NAME, convert_sklearn_type(onnx_shapes))]

    try:
        # sklearn2onnx converts RandomForestClassifier to an estimator with
        # a "ZipMap" operator. The option "zipmap":false disables the zipmap operator
        # http://onnx.ai/sklearn-onnx/auto_examples/plot_convert_zipmap.html
        # See open issue: https://github.com/onnx/sklearn-onnx/issues/816
        onnx_model = convert_sklearn(
            sklearn_model, initial_types=initial_type, options={"zipmap": False}
        )
    except MissingShapeCalculator as e:
        raise e
    except NameError:
        onnx_model = convert_sklearn(sklearn_model, initial_types=initial_type)
    except Exception:
        print("Attempting conversion of XGBoost model to ONNX")
        try:
            onnx_model = onnxmltools.convert.convert_xgboost(
                sklearn_model, initial_types=initial_type
            )
        except Exception as e:
            print("Exception converting scikit-learn model to ONNX.")
            raise e
    print(f"Converted sklearn model {sklearn_model} to ONNX")
    return onnx_model


def train_model(num_features, model_class):
    X, y = make_blobs(n_samples=1000,
               n_features=num_features,
               cluster_std=1.5,
               random_state=1)
    model = model_class()
    model.fit(X, y)
    onnx_model = sklearn_to_onnx(model, X.dtype, num_features)
    print(f"Trained a model {model_class}")
    return model, onnx_model


def benchmark_sklearn(model, samples_per_batch, benchmark_runs=10):
    input_data = np.ones((samples_per_batch, model.n_features_in_))
    numbers = []
    for i in range(benchmark_runs):
        now = time.time()
        model.predict(input_data)
        later = time.time()
        elapsed = later - now
        numbers.append(elapsed)
    result = sum(numbers) / benchmark_runs
    print(f"Benchmarked sklearn model {result}")
    return result

def benchmark_onnx(model, samples_per_batch, num_features, ort_sess, benchmark_runs=10):
    input_data = np.ones((samples_per_batch, num_features), dtype=np.float32)
    numbers = []
    ort_run_options = RunOptions()
    ort_run_options.log_severity_level = 1
    for i in range(benchmark_runs):
        now = time.time()
        ort_sess.run(None, {'input': input_data}, run_options=ort_run_options)
        later = time.time()
        elapsed = later - now
        numbers.append(elapsed)
    result = sum(numbers) / benchmark_runs
    print(f"Benchmarked onnx model {result}")
    return result

def get_model_class_name(model_class):
    class_str = str(model_class)
    return class_str[class_str.find("'") + 1:class_str.rfind("'")]

if __name__=="__main__":
    initialize()
    # Model with one feature.
    results = []
    model_classes = [xgb.XGBClassifier,
                    xgb.XGBRegressor,
                    KNeighborsClassifier,
                    MLPClassifier,
                    MLPRegressor,
                    SVC,
                    DecisionTreeClassifier,
                    LogisticRegression,
                    LinearRegression,
                    BernoulliNB
                    ]

    models = [(get_model_class_name(model_class), model_class) for model_class in model_classes]
    num_features = [1, 10, 100, 1000]
    num_samples = [1, 10, 100, 1000, 10000]
    for features in num_features:
        for samples in num_samples:
            for model_name, model_type in models:

                sklearn_model, onnx_model = train_model(features, model_type)
                sklearn_latency  = benchmark_sklearn(sklearn_model, samples)

                ort_sess_cpu = ort.InferenceSession(onnx_model.SerializeToString(), providers=["CPUExecutionProvider"])
                onnx_latency_cpu  = benchmark_onnx(onnx_model, samples, features, ort_sess_cpu)

                ort_sess_openvino = ort.InferenceSession(onnx_model.SerializeToString(), providers=["OpenVINOExecutionProvider"])
                onnx_latency_openvino  = benchmark_onnx(onnx_model, samples, features, ort_sess_openvino)

                results.append([model_name, 'sklearn', 'n/a', features, samples, sklearn_latency])
                results.append([model_name, 'onnx', 'CPUExecutionProvider', features, samples, onnx_latency_cpu])
                results.append([model_name, 'onnx', 'OpenVINOExecutionProvider', features, samples, onnx_latency_openvino])
    result_str = "model_name,model_type,execution_provider,num_features,num_samples,latency\n"
    for result_line in results:
        s = f"{result_line[0]},{result_line[1]},{result_line[2]},{result_line[3]},{result_line[4]},{result_line[5]}\n"
        result_str += s
    with open("results7.csv", "w") as f:
        f.write(result_str)

